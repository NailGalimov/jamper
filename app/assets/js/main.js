// Проверка на ios
var iOS = /iPad|iPhone|iPod/.test(navigator.platform || "");
if (iOS) {
	$('body').addClass('ios');
}

function setViewport() {
    var mvp = $('#myViewport');
    if (window.matchMedia("(max-width: 360px)").matches) {
        mvp.attr('content', 'user-scalable=no, width=360');
    } else {
        mvp.attr('content', 'width=device-width');
    }
};

$(window).resize(function () {
    setViewport()
});


$(function () {
	setViewport();

	var myLazyLoad = new LazyLoad();

	myLazyLoad.update();


	// аккордион
	$(document).on("click", ".js-accordion-open", function(){
		var parent = $(this).closest(".accordion__item");
		var accordionHead = parent.find(".accordion__item_head");
		var content = parent.find(".accordion__item_info");
		var contentHeight = content.prop("scrollHeight");

		$(".accordion__item").removeClass("active");
		$(".accordion__item_info").css("max-height", "0");
		$(".accordion__item_info").removeClass("accordion--content_visible");
		parent.toggleClass("active");

		if(parent.hasClass("active")) {
			accordionHead.attr("aria-expanded", "true");
			content.attr("aria-hidden", "false");

			content.css("max-height", contentHeight);
		} else {
			accordionHead.attr("aria-expanded", "false");
			content.attr("aria-hidden", "true");

			content.css("max-height", "0");
		}

	});

	// навигационная линия
	const nav = $(".header-custom__nav"),
	      navLine = $(".nav-line"),
		  navItem = $(".header-custom__nav ul li");
	var navItemFirst = navItem.eq(0);

	navLine.css("width", navItemFirst.outerWidth());

	navItem.each(function(el) {

		$(this).on("mouseenter", function(e){
			var currentTarget = e.currentTarget;
			var currentTargetWidth = $(currentTarget).outerWidth();
			var offsetLeftCurrentTarget = currentTarget.offsetLeft;

			navLine.css("width", currentTargetWidth);
			navLine.css("left", offsetLeftCurrentTarget);
		});
	});

	// клиенты слайдер
	$(".clients__container").slick({
		slidesToShow: 3,
		slidesToScroll: 1,
		dots: false,
		arrows: false,
		autoplay: true,
		autoplaySpeed: 100,
		pauseOnFocus: false,
		pauseOnHover: false,
		speed: 30000,
		variableWidth: true,
		responsive: [
			{
			breakpoint: 1281,
				settings: {
					slidesToShow: 2,
				}
			},
			{
			breakpoint: 992,
				settings: {
					slidesToShow: 2,
					autoplaySpeed: 300,
					speed: 20000,
				}
			},
			{
			breakpoint: 770,
				settings: {
					slidesToShow: 4,
					autoplaySpeed: 300,
					speed: 20000,
				}
			},
		]
	})


	// табы
	$(document).on('click', '.tabs [data-show]:not(.active):not(.accordion_tab)', function () {

		var container = $(this).closest('.tabs'),
			toShow = $(this).attr('data-show') || 1;


		container.find('[data-show]').removeClass('active');
		$(this).addClass('active');

		container.find('[data-item]').removeClass('active');
		container.find('[data-item="' + toShow + '"]').addClass('active');

		container.find('[data-item]').hide();
		container.find('[data-item="' + toShow + '"]').fadeIn(500, function () {

		});

		container.find('.tabs [data-item]').hide();
		container.find('.tabs [data-item]:not(.hidden)').eq(0).show();
		// container.find('.tabs .tabs__radio').eq(0).prop("checked", true);

		// container.find('.slick-slider').slick('setPosition');
	});


	// анимация блоков services
	var timeline = gsap.timeline({
		scrollTrigger: {
			trigger: '.customization',
			// scrub: true,
			// pin: true,
			// scrub: 1,
			start: "top center"
		}
	});

	var rule1 = CSSRulePlugin.getRule(".services__col:nth-child(2)::before");
	var rule2 = CSSRulePlugin.getRule(".services__col:nth-child(1)::before");

	gsap.to(rule1, {
		duration: 1,
		cssRule: { x: 0, y: 42, rotation: 9},
		// ease: Power2.easeInOut,
		scrollTrigger: {
			trigger: '.services',
			start: "center bottom"
		}
	});
	gsap.to(rule2, {
		duration: 1,
		cssRule: { x: -15, y: 51, rotation: -9},
		// ease: Power2.easeInOut,
		scrollTrigger: {
			trigger: '.services',
			start: "center bottom"
		}
	});

	if (window.matchMedia("(min-width: 1200px)").matches) {
		// анимация футболок
		function runOnComplete() {
			$(".customization__item").removeClass("--events-none");
			$(".customization__item").addClass("main-transition")
		}

		timeline.fromTo(".customization__img", {
			x: "100%",
			y: 25,
			opacity: 0
		}, {
			x: 0,
			y: 0,
			opacity: 1,
			duration: 0.8,
		})
		.fromTo(".customization__item:nth-child(1)", {
			x: "100%",
			y: 25,
			opacity: 0
		}, {
			x: 0,
			y: 0,
			opacity: 1,
			duration: 0.8,
		},)
		.fromTo(".customization__item:nth-child(2)", {
			x: "100%",
			y: 25,
			opacity: 0
		}, {
			x: 0,
			y: 0,
			opacity: 1,
			duration: 0.8,
		},"-=0.5")
		.fromTo(".customization__item:nth-child(3)", {
			x: "100%",
			y: 25,
			opacity: 0
		}, {
			x: 0,
			y: 0,
			opacity: 1,
			duration: 0.8,
		},"-=0.5")
		.fromTo(".customization__item:nth-child(4)", {
			x: "100%",
			y: 25,
			opacity: 0
		}, {
			x: 0,
			y: 0,
			opacity: 1,
			duration: 0.8,
			onComplete: runOnComplete
		},"-=0.5")
	}

	// открытие меню в мобилке
	let mobileTimeLine = gsap.timeline({
		paused: true,
		reversed: true
	})

	mobileTimeLine
		.set(".menu_mobile", {top: 0})
		.to(".menu_mobile", {
			duration: 0.7,
			width: "100%",
			ease: Power4.easeInOut,
		})
		.to(".menu_mobile__nav ul li", {
			x: 0,
			opacity: 1,
			duration: 0.7
		})
		.to(".menu_mobile__mail-link", {
			y: 0,
			opacity: 1,
			duration: 0.5
		}, "-=0.3")
		.to(".menu_mobile__tel", {
			y: 0,
			opacity: 1,
			duration: 0.5
		}, "-=0.3")
		.to(".menu_mobile__btn", {
			y: 0,
			opacity: 1,
			duration: 0.5
		}, "-=0.3")

	$(document).on("click", ".js-open_menu", function(e){

		$(this).toggleClass("active");

		if (mobileTimeLine.isActive()) {
			e.preventDefault();
			return false;
		}

		isReversed(mobileTimeLine);
	});

	function isReversed(animation) {
		animation.reversed() ? animation.play() : animation.reverse();
	}

	// навигация блока сотрудничество
	$(document).on("click", ".js--arrow-right", function() {
		var th = $(this);
		var container = th.closest(".cooperation__row");
		var itemActive = container.find(".cooperation__info_item.active");
		var item = container.find(".cooperation__info_item");
		var stepsActiveMobile = container.find(".cooperation__nav.-mobile .cooperation__nav_item.active");
		var stepsActive = container.find(".cooperation__nav.js--not-mobile .cooperation__nav_item.active");
		var currentNumberActive = container.find(".cooperation__numbers_item.active");

		itemActive.removeClass("active");
		itemActive.next().eq(0).addClass("active");

		stepsActive.removeClass("active");
		stepsActive.next().eq(0).addClass("active");

		stepsActiveMobile.removeClass("active");
		stepsActiveMobile.next().eq(0).addClass("active");

		currentNumberActive.removeClass("active");
		currentNumberActive.next().eq(0).addClass("active");

		$(".js--arrow-left").removeClass("disabled");


		if (item.eq(-1).hasClass("active")) {
			$(".js--arrow-right").addClass("disabled");
		}


	});

	$(document).on("click", ".js--arrow-left", function() {
		var th = $(this);
		var container = th.closest(".cooperation__row");
		var itemActive = container.find(".cooperation__info_item.active");
		var item = container.find(".cooperation__info_item");
		var stepsActiveMobile = container.find(".cooperation__nav.-mobile .cooperation__nav_item.active");
		var stepsActive = container.find(".cooperation__nav.js--not-mobile .cooperation__nav_item.active");

		var currentNumberActive = container.find(".cooperation__numbers_item.active");

		itemActive.removeClass("active");
		itemActive.prev().eq(-1).addClass("active");

		stepsActive.removeClass("active");
		stepsActive.prev().eq(-1).addClass("active");

		currentNumberActive.removeClass("active");
		currentNumberActive.prev().eq(-1).addClass("active");

		stepsActiveMobile.removeClass("active");
		stepsActiveMobile.prev().eq(-1).addClass("active");

		$(".js--arrow-right").removeClass("disabled");

		if (item.eq(0).hasClass("active")) {
			$(".js--arrow-left").addClass("disabled");
		}
	});

	$(document).on("click", ".cooperation__nav_item:last-child", function(){
		$(".js--arrow-left").removeClass("disabled");
		$(".js--arrow-right").addClass("disabled");
	});
	$(document).on("click", ".cooperation__nav_item:first-child", function(){
		$(".js--arrow-right").removeClass("disabled");
		$(".js--arrow-left").addClass("disabled");
	});
	$(document).on("click", ".cooperation__nav_item:not(:first-child, :last-child)", function(){
		$(".arrow-number").removeClass("disabled");
	});
	$(document).on("click", ".cooperation__nav_item", function(){
		var container = $(this).closest(".cooperation__row");
		var parentAttr = $(this).attr("data-nav-parent");

		container.find(".cooperation__numbers_item").removeClass("active");
		container.find(".cooperation__numbers_item[data-nav-child="+ parentAttr +"]").addClass("active");

	});



});
