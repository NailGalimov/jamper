<!DOCTYPE html>
<html lang="ru">
<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta id="myViewport" name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Jamper</title>

	<!-- <link rel="apple-touch-icon" sizes="120x120" href="include/favicon/apple-touch-icon.png">
	<link rel="icon" type="image/png" sizes="32x32" href="include/favicon/favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="16x16" href="include/favicon/favicon-16x16.png">
	<link rel="manifest" href="include/favicon/site.webmanifest">
	<link rel="mask-icon" href="include/favicon/safari-pinned-tab.svg" color="#5bbad5">
	<meta name="msapplication-TileColor" content="#da532c">
	<meta name="theme-color" content="#ffffff"> -->

	<link rel="stylesheet" href="assets/css/libs.style.css">
    <link rel="stylesheet" href="assets/fonts/icomoon/style.css">
    <link rel="stylesheet" href="assets/css/base.min.css">
    <link rel="stylesheet" href="assets/css/style.css">
</head>

<body class="body-custom">

	<header class="header-custom">
		<div class="header-custom__top">
			<div class="container-fluid">
				<div class="flex-container">
					<a href="#" class="header-custom__logo main-logo">
						<img src="./assets/images/main-logo.png" alt="Логотип">
						<p>
							Пошив уникальной спортивной одежды
						</p>
					</a>

					<div class="header-custom__mail">
						<a href="#" class="main-mail header-custom__mail-link">
							jamper.kazan@yandex.ru
						</a>
					</div>

					<a href="#" class="header-custom__tel main-tel">
						+7 (906) 323-43-95
					</a>

					<button class="header-custom__btn btn">
						<span>Заказать звонок</span>
					</button>
				</div>
			</div>
		</div>
		<div class="header-custom__bottom">
			<div class="container-fluid">
				<nav class="header-custom__nav">
					<span class="nav-line"></span>
					<ul>
						<li><a href="#">О компании</a></li>
						<li><a href="#">Продукция</a></li>
						<li><a href="#">Одежда оптом</a></li>
						<li><a href="#">Нанесения</a></li>
						<li><a href="#">Доставка</a></li>
						<li><a href="#">Контакты</a></li>
					</ul>
				</nav>
			</div>
		</div>

	</header>

	<main class="main_custom">
	<button class="mobile_btn js-open_menu">
		<span></span>
		<span></span>
		<span></span>
	</button>

	<div class="menu_mobile">
		<div class="menu_mobile__inner">
			<nav class="menu_mobile__nav">
				<ul>
					<li><a href="#">О компании</a></li>
					<li><a href="#">Продукция</a></li>
					<li><a href="#">Одежда оптом</a></li>
					<li><a href="#">Нанесения</a></li>
					<li><a href="#">Доставка</a></li>
					<li><a href="#">Контакты</a></li>
				</ul>
			</nav>

			<a href="#" class="main-mail menu_mobile__mail-link">
				jamper.kazan@yandex.ru
			</a>

			<a href="#" class="menu_mobile__tel main-tel">
				+7 (906) 323-43-95
			</a>

			<button class="menu_mobile__btn btn">
				<span>Заказать звонок</span>
			</button>

		</div>
	</div>
