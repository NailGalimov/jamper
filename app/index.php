<? include $_SERVER['DOCUMENT_ROOT'].'/app/header.php'?>

<section class="section hero_section" style="background-image: url(./assets/images/webp/hero-img.webp)">
	<div class="hero_section__img">
		<img class="lazy" data-src="./assets/images/webp/hero-img-mobile.webp" alt="Девушка">
	</div>
	<div class="hero_section__inner">
		<div class="container-fluid">

			<div class="hero_section__title">
				<h1>
					Шьем модную <span>корпоративную одежду</span>
					<strong>из премиум тканей на заказ</strong>
				</h1>

			</div>

			<button class="hero_section__btn btn"><span>Рассчитать стоимость</span></button>

			<div class="hero_section__icons">
				<p class="hero_section__icons_text">Доставка в любую точку России</p>
				<p class="hero_section__icons_text">Оптовые цены <br> при заказе – от 5 штук</p>
			</div>

		</div>
	</div>
</section>

<section class="section feautures">
	<div class="feautures__image">

		<picture>
			<source data-srcset="./assets/images/webp/feautures_girl.webp" type="image/webp">
			<source data-srcset="./assets/images/feautures_girl.png" type="image/png">
			<img class="lazy" data-src="./assets/images/feautures_girl.png" alt="Девушка">
		</picture>

	</div>

	<div class="feautures__first_line feautures__line">
		<img class="lazy" data-src="./assets/images/first_line.svg" alt="Линия">
	</div>

	<div class="feautures__second_line feautures__line">
		<img class="lazy" data-src="./assets/images/second_line.svg" alt="Линия">
	</div>

	<div class="feautures__third_line feautures__line">
		<img class="lazy" data-src="./assets/images/third_line.svg" alt="Линия">
	</div>

	<div class="feautures__fourth_line feautures__line">
		<img class="lazy" data-src="./assets/images/fourth_line.svg" alt="Линия">
	</div>

	<div class="container-fluid">
		<div class="feautures__row">

			<div class="feautures__col">

				<div class="feautures_box">
					<div class="feautures_box__image">
						<img class="lazy" data-src="./assets/images/feautures_img1.png" alt="">
					</div>
					<div class="feautures_box__text">
						<h4>
							<span>5 лет</span> <br> на рынке
						</h4>

						<p>
							заряжаем командным духом
						</p>
					</div>
				</div>
				<div class="feautures_box">
					<div class="feautures_box__image">
						<img class="lazy" data-src="./assets/images/feautures_img3.png" alt="">
					</div>
					<div class="feautures_box__text">
						<h4>
							Оденем команду <br> из 30 человек <span>за 3 недели</span>
						</h4>

						<p>
							экипируем хоть олимпийскую сборную
						</p>
					</div>
				</div>

			</div>

			<div class="feautures__col">

				<div class="feautures_box">
					<div class="feautures_box__image">
						<img class="lazy" data-src="./assets/images/feautures_img2.png" alt="">
					</div>
					<div class="feautures_box__text">
						<h4>
							Собственное <span>производство</span>
						</h4>

						<p>
							работаем четко, как швейная машинка Singer
						</p>
					</div>
				</div>
				<div class="feautures_box">
					<div class="feautures_box__image">
						<img class="lazy" data-src="./assets/images/feautures_img4.png" alt="">
					</div>
					<div class="feautures_box__text">
						<h4>
							Экипировали <br> <span>более 100 команд</span>
						</h4>

						<p>
							с учетом параметров каждого участника
						</p>
					</div>
				</div>
			</div>

		</div>
	</div>
</section>

<section class="section about_company">
	<div class="about_company__col_image">
		<img src="./assets/images/about_company_bg1.png" alt="">
	</div>
	<div class="about_company__col_image --bottom">
		<img src="./assets/images/about_company_bg1.png" alt="">
	</div>

	<div class="about_company__image">
		<img class="lazy" data-src="./assets/images/about_company__img.png" alt="">
	</div>


	<div class="container-fluid">
		<div class="about_company__row tabs">
			<div class="about_company__col">
				<div class="about_company__title">
					<h2>
						Корпоративный стиль
					</h2>
					<p>
						объединяет команду и повышает авторитет компании в глазах клиентов
					</p>
				</div>

				<ul class="tabs__nav -mobile">
					<li class="tabs__nav_item active" data-show="1">Костюмы</li>
					<li class="tabs__nav_item" data-show="2">Худи</li>
					<li class="tabs__nav_item" data-show="3">оверсайз-футболки</li>
					<li class="tabs__nav_item" data-show="4">Свитшоты</li>
					<li class="tabs__nav_item" data-show="5">детские костюмы</li>
				</ul>


				<div class="about_company__tabs_list tabs__list">

					<div class="tabs__item active" data-item="1">

						<div class="tabs__item_info">
							<h4>
								Костюмы
							</h4>
							<p>
								Полная кастомизация вашей команды!
								80+ комбинаций костюмов, которые заряжены на боевой настрой.
							</p>

							<a href="#" class="btn tabs__item_btn">
								<span>Перейти в каталог</span>
							</a>
						</div>

						<div class="tabs__item_img">
							<picture>
								<source data-srcset="./assets/images/webp/tabs_item_img1.webp" type="image/webp">
								<source data-srcset="./assets/images/tabs_item_img1.png" type="image/png">
								<img class="lazy" data-src="./assets/images/tabs_item_img1.png" alt="Костюмы">
							</picture>
						</div>

					</div>
					<div class="tabs__item" data-item="2">

						<div class="tabs__item_info">
							<h4>
								Худи
							</h4>
							<p>
								Классные худи для тех, кто ценит стиль и качество.
								Премиум-ткань долго сохраняет цвет и форму изделия.
								А худи с начесом греют не только тело, но и душу.
							</p>

							<a href="#" class="btn tabs__item_btn">
								<span>Перейти в каталог</span>
							</a>
						</div>

						<div class="tabs__item_img">
							<picture>
								<source data-srcset="./assets/images/webp/tabs_item_img2.webp" type="image/webp">
								<source data-srcset="./assets/images/tabs_item_img2.png" type="image/png">
								<img class="lazy" data-src="./assets/images/tabs_item_img2.png" alt="Худи">
							</picture>
						</div>

					</div>
					<div class="tabs__item" data-item="3">

						<div class="tabs__item_info">
							<h4>
								оверсайз-футболки
							</h4>
							<p>
								Для самых активных парней и девчонок
								оверсайз-футболки из кулирной глади. Хлопок в
								составе позволяет коже свободно «дышать», а
								лайкра не даст любимой футболке растянуться или сесть.
							</p>

							<a href="#" class="btn tabs__item_btn">
								<span>Перейти в каталог</span>
							</a>
						</div>

						<div class="tabs__item_img">
							<picture>
								<source data-srcset="./assets/images/webp/tabs_item_img.webp" type="image/webp">
								<source data-srcset="./assets/images/tabs_item_img.png" type="image/png">
								<img class="lazy" data-src="./assets/images/tabs_item_img.png" alt="оверсайз-футболки">
							</picture>
						</div>

					</div>
					<div class="tabs__item" data-item="4">

						<div class="tabs__item_info">
							<h4>
								Свитшоты
							</h4>
							<p>
								Свитшоты идут всем!
								Это стильное и качественное решение для вашего бизнеса.
								Трехниточный турецкий футер качества “пенье” и уникальный
								дизайн помогут вашей компании попасть в самое сердце клиентов.
							</p>

							<a href="#" class="btn tabs__item_btn">
								<span>Перейти в каталог</span>
							</a>
						</div>

						<div class="tabs__item_img">
							<picture>
								<source data-srcset="./assets/images/webp/tabs_item_img4.webp" type="image/webp">
								<source data-srcset="./assets/images/tabs_item_img4.png" type="image/png">
								<img class="lazy" data-src="./assets/images/tabs_item_img4.png" alt="Свитшоты">
							</picture>
						</div>

					</div>
					<div class="tabs__item" data-item="5">

						<div class="tabs__item_info">
							<h4>
								детские костюмы
							</h4>
							<p>
								Турецкий футер премиум качества идеален
								для чувствительной детской кожи.
								Тело дышит, ткань не боится частых стирок,
								а правильно подобранный размер не будет стеснять
								движения юных спортсменов!
							</p>

							<a href="#" class="btn tabs__item_btn">
								<span>Перейти в каталог</span>
							</a>
						</div>

						<div class="tabs__item_img">
							<picture>
								<source data-srcset="./assets/images/webp/tabs_item_img5.webp" type="image/webp">
								<source data-srcset="./assets/images/tabs_item_img5.png" type="image/png">
								<img class="lazy" data-src="./assets/images/tabs_item_img5.png" alt="детские костюмы">
							</picture>
						</div>

					</div>

				</div>
			</div>
			<div class="about_company__col">
				<ul class="tabs__nav">
					<li class="tabs__nav_item active" data-show="1">Костюмы</li>
					<li class="tabs__nav_item" data-show="2">Худи</li>
					<li class="tabs__nav_item" data-show="3">оверсайз-футболки</li>
					<li class="tabs__nav_item" data-show="4">Свитшоты</li>
					<li class="tabs__nav_item" data-show="5">детские костюмы</li>
				</ul>
			</div>
		</div>
	</div>


</section>

<section class="section company_catalog">


	<div class="container-fluid">
		<div class="company_catalog__title">
			<h2>
				Наш <span>ассортимент</span>
			</h2>
		</div>

		<div class="company_catalog__container">
			<?
				$array = ["Худи", "Бомберы", "Свитшоты", "Спортивные костюмы", "Футболки и лонгсливы", "Худи оверсайз", "Удлиненные худи", "Детские костюмы", "Толстовки на молнии", "Футболки поло"];
				$number = 0;
			?>

			<?php foreach( $array as $array_item ): ?>
				<? $number++; ?>

				<div class="company_catalog__col">
					<a href="" class="company_catalog__item catalog_item">
						<div class="catalog_item__top">
							<div class="catalog_item__img">
								<picture>
									<source data-srcset="./assets/images/webp/catalog_item_img<? echo $number;?>.webp" type="image/webp">
									<source data-srcset="./assets/images/catolg_images/catalog_item_img<? echo $number;?>.png" type="image/png">
									<img class="lazy" data-src="./assets/images/catolg_images/catalog_item_img<? echo $number;?>.png" alt="Костюмы">
								</picture>
							</div>
						</div>
						<div class="catalog_item__text">
							<? echo $array_item; ?>
						</div>
					</a>
				</div>

			<?php endforeach; ?>
		</div>

		<a href="#" class="company_catalog__btn btn">
			<span>Еще</span>
		</a>
	</div>
</section>

<section class="section services lazy" data-bg="./assets/images/srvises_bg.jpg">
	<div class="services__top_bg">
		<img src="./assets/images/services_bg1.png" alt="#">
	</div>

	<div class="container-fluid">
		<div class="services__main_img">
			<picture>
				<source data-srcset="./assets/images/webp/services__image.webp" type="image/webp">
				<source data-srcset="./assets/images/services__image.png" type="image/png">
				<img class="lazy" data-src="./assets/images/services__image.png" alt="Девушка-швея">
			</picture>
		</div>

		<div class="services__title">
			<h2>
				Швеи с опытом <span>от 10 лет</span>
			</h2>
			<p>
				и еще <span>6 причин заказать у нас</span> апгрейд корпоративного стиля
			</p>
		</div>

		<div class="services__row">
			<div class="services__col">
				<ul class="accordion">
					<li class="services__item accordion__item active ">
						<div class="accordion__item_head js-accordion-open" data-text="1" aria-expanded="false">
							Качественные ткани
							<div class="icon-icon-arrow-top accordion__icon"></div>
						</div>
						<div class="accordion__item_info accordion--content_visible" aria-hidden="true">
							<p>
								Ткани, которые мы используем, делают
								спортивную одежду комфортной.
								Изделия долго сохраняют форму и цвет, неприхотливы в уходе и
								не боятся частых стирок.
							</p>
						</div>
					</li>
					<li class="services__item accordion__item">
						<div class="accordion__item_head js-accordion-open" data-text="2" aria-expanded="false">
							Гибкий подход к изделиям
							<div class="icon-icon-arrow-top accordion__icon"></div>
						</div>
						<div class="accordion__item_info" aria-hidden="true">
							<p>
								Ткани, которые мы используем, делают
								спортивную одежду комфортной.
								Изделия долго сохраняют форму и цвет, неприхотливы в уходе и
								не боятся частых стирок.
							</p>
						</div>
					</li>
					<li class="services__item accordion__item" >
						<div class="accordion__item_head js-accordion-open" data-text="3" aria-expanded="false">
							Одежда точно по размеру
							<div class="icon-icon-arrow-top accordion__icon"></div>
						</div>
						<div class="accordion__item_info" aria-hidden="true">
							<p>
								Ткани, которые мы используем, делают
								спортивную одежду комфортной.
								Изделия долго сохраняют форму и цвет, неприхотливы в уходе и
								не боятся частых стирок.
							</p>
						</div>
					</li>
				</ul>

			</div>

			<div class="services__col">

				<ul class="accordion">
					<li class="services__item accordion__item">
						<div class="accordion__item_head js-accordion-open" data-text="4" aria-expanded="false">
								Дизайн любой сложности
							<div class="icon-icon-arrow-top accordion__icon"></div>
						</div>
						<div class="accordion__item_info" aria-hidden="true">
							<p>
								Ткани, которые мы используем, делают
								спортивную одежду комфортной.
								Изделия долго сохраняют форму и цвет, неприхотливы в уходе и
								не боятся частых стирок.
							</p>
						</div>
					</li>
					<li class="services__item accordion__item" >
						<div class="accordion__item_head js-accordion-open" data-text="5" aria-expanded="false">
							Все заботы берем на себя
							<div class="icon-icon-arrow-top accordion__icon"></div>
						</div>
						<div class="accordion__item_info" aria-hidden="true">
							<p>
								Ткани, которые мы используем, делают
								спортивную одежду комфортной.
								Изделия долго сохраняют форму и цвет, неприхотливы в уходе и
								не боятся частых стирок.
							</p>
						</div>
					</li>
					<li class="services__item accordion__item">
						<div class="accordion__item_head js-accordion-open" data-text="6" aria-expanded="false">
							Работаем без посредников
							<div class="icon-icon-arrow-top accordion__icon"></div>
						</div>
						<div class="accordion__item_info" aria-hidden="true">
							<p>
								Ткани, которые мы используем, делают
								спортивную одежду комфортной.
								Изделия долго сохраняют форму и цвет, неприхотливы в уходе и
								не боятся частых стирок.
							</p>
						</div>
					</li>
				</ul>

			</div>
		</div>


	</div>
</section>

<section class="section customization">
	<div class="customization__img">
		<img class="lazy" data-src="./assets/images/customization__img.png" alt="Нить с иглой">
	</div>
	<div class="customization__img--mobile">
		<img class="lazy" data-src="./assets/images/customization/line-mobile.png" alt="Нить с иглой">
	</div>
	<div class="customization__title">
		<h2>
			Полная <span>кастомизация <br> корпоративного стиля</span> на всех уровнях
		</h2>
	</div>

	<div class="customization__container--mobile">
		<div class="customization__item--mobile">
			<div class="customization__image">
				<img class="lazy" data-src="./assets/images/customization/customization__image.png" alt="Футболка">

				<div class="customization__text">
					<h4>
						Проверенные лекала
					</h4>
					<p>
						Шьем одежду по универсальным лекалам. Сидит по фигуре, удобна, долговечна и выглядит стильно
					</p>
				</div>

			</div>
		</div>
		<div class="customization__item--mobile">
			<div class="customization__image">
				<img class="lazy" data-src="./assets/images/customization/customization__image2.png" alt="Футболка">

				<div class="customization__text">
					<h4>
						Разработка дизайна
					</h4>
					<p>
						Бесплатно разработаем 3-4 дизайн-макета на выбор или воплотим в жизнь вашу задумку.
					</p>
				</div>

			</div>
		</div>
		<div class="customization__item--mobile">
			<div class="customization__image">
				<img class="lazy" data-src="./assets/images/customization/customization__image3.png" alt="Футболка">

				<div class="customization__text">
					<h4>
						4 варианта нанесения
					</h4>
					<p>
						Нанесем дизайн любой сложности.
						Предложим 4 варианта под ваш вкус и бюджет.
					</p>
				</div>
			</div>
		</div>
		<div class="customization__item--mobile">
			<div class="customization__image">
				<img class="lazy" data-src="./assets/images/customization/customization__image4.png" alt="Футболка">

				<div class="customization__text">
					<h4>
						Индивидуальный пошив
					</h4>
					<p>
						Сошьем изделие по индивидуальным меркам для тех, кому не подходит стандартная размерная сетка.
					</p>
				</div>
			</div>
		</div>
	</div>



	<div class="customization__container">
		<div class="customization__item --events-none">
			<div class="customization__image">
				<img class="lazy" data-src="./assets/images/customization/customization__image.png" alt="Футболка">

				<div class="customization__text">
					<h4>
						Проверенные лекала
					</h4>
					<p>
						Шьем одежду по универсальным лекалам. Сидит по фигуре, удобна, долговечна и выглядит стильно
					</p>
				</div>

			</div>
		</div>
		<div class="customization__item --events-none">
			<div class="customization__image">
				<img class="lazy" data-src="./assets/images/customization/customization__image2.png" alt="Футболка">

				<div class="customization__text">
					<h4>
						Разработка дизайна
					</h4>
					<p>
						Бесплатно разработаем 3-4 дизайн-макета на выбор или воплотим в жизнь вашу задумку.
					</p>
				</div>

			</div>
		</div>
		<div class="customization__item --events-none">
			<div class="customization__image">
				<img class="lazy" data-src="./assets/images/customization/customization__image3.png" alt="Футболка">

				<div class="customization__text">
					<h4>
						4 варианта нанесения
					</h4>
					<p>
						Нанесем дизайн любой сложности.
						Предложим 4 варианта под ваш вкус и бюджет.
					</p>
				</div>
			</div>
		</div>
		<div class="customization__item --events-none">
			<div class="customization__image">
				<img class="lazy" data-src="./assets/images/customization/customization__image4.png" alt="Футболка">

				<div class="customization__text">
					<h4>
						Индивидуальный пошив
					</h4>
					<p>
						Сошьем изделие по индивидуальным меркам для тех, кому не подходит стандартная размерная сетка.
					</p>
				</div>
			</div>
		</div>
	</div>

	<div class="customization__btn">
		<button class="btn">
			<span>Получить оптовый прайс</span>
		</button>
	</div>


</section>

<section class="section fabrics">

	<div class="fabrics__top_img">
		<img src="./assets/images/fabrics__img.png" alt="Рулетка">
	</div>

	<div class="fabrics__title">
		<h2>
			Используем <span>премиум ткани</span> <br>
			с содержанием хлопка – от 70%
		</h2>
	</div>

	<div class="fabrics__container">
		<div class="fabrics__image">
			<img src="./assets/images/services_bg1.png" alt="Линия вышивки">
		</div>

		<div class="fabrics__item lazy" data-bg="./assets/images/fabrics_bg_img.jpg">

			<div class="fabrics__text">
				<h4>
					Футер
				</h4>
				<p>
					Ткань, которая идеальна для пошива спортивных костюмов: гипоаллергенна, позволяет коже “дышать”, выводит влагу и сохраняет тепло.
				</p>
				<div class="fabrics__hidden_text">
					<p><strong>Качество:</strong> пенье.</p>
					<p><strong>Состав:</strong> 93% хлопка, 7% лайкры.</p>
					<p><strong>Плотность:</strong> 190-200 г/м3.</p>
				</div>
			</div>

			<div class="fabrics__img">
				<img class="lazy" data-src="./assets/images/fabrics_img3.jpg" alt="Кулирная гладь">
			</div>

		</div>

		<div class="fabrics__item lazy" data-bg="./assets/images/fabrics_bg_img2.jpg">
			<div class="fabrics__text">
				<h4>
					Футер трехнитка с начесом
				</h4>
				<p>
					Благодаря особой технологии, ткань на изнанке можно распушить и получить начес. Такое изделие становится очень мягким и теплым.
				</p>
				<div class="fabrics__hidden_text">
					<p><strong>Качество:</strong> пенье.</p>
					<p><strong>Состав:</strong> 93% хлопка, 7% лайкры.</p>
					<p><strong>Плотность:</strong> 190-200 г/м3.</p>
				</div>
			</div>

			<div class="fabrics__img">
				<img class="lazy" data-src="./assets/images/fabrics_img3.jpg" alt="Кулирная гладь">
			</div>
		</div>

		<div class="fabrics__item lazy" data-bg="./assets/images/fabrics_bg_img3.jpg">
			<div class="fabrics__text">
				<h4>
					Кулирная гладь
				</h4>
				<p>
					Применяется для пошива футболок, поло и лонгсливов. Мы используем ткань плотностью не менее 190 г/м3. Поэтому изделия получаются долговечными и приятными к телу.
				</p>
				<div class="fabrics__hidden_text">
					<p><strong>Качество:</strong> пенье.</p>
					<p><strong>Состав:</strong> 93% хлопка, 7% лайкры.</p>
					<p><strong>Плотность:</strong> 190-200 г/м3.</p>
				</div>
			</div>

			<div class="fabrics__img">
				<img class="lazy" data-src="./assets/images/fabrics_img3.jpg" alt="Кулирная гладь">
			</div>
		</div>

		<div class="fabrics__item lazy" data-bg="./assets/images/fabrics_bg_img4.jpg">
			<div class="fabrics__text">
				<h4>
					Пике
				</h4>
				<p>
					Благодаря особому переплетению нитей и фактуры ткани, пике идеальна для создания футболок и платьев поло. Это экологичный материал, который позволяет коже "дышать".
				</p>
				<div class="fabrics__hidden_text">
					<p><strong>Качество:</strong> пенье.</p>
					<p><strong>Состав:</strong> 93% хлопка, 7% лайкры.</p>
					<p><strong>Плотность:</strong> 190-200 г/м3.</p>
				</div>
			</div>

			<div class="fabrics__img">
				<img class="lazy" data-src="./assets/images/fabrics_img3.jpg" alt="Кулирная гладь">
			</div>
		</div>
	</div>
</section>

 <section class="section clients">
	<div class="clients__title">
		<h2>
			Одели <span>более 100 корпоративных клиентов</span> и спортивных команд
		</h2>
	</div>

	<div class="clients__img">
		<img src="./assets/images/clients_bg.png" alt="Иголка с ниткой">
	</div>


	<div class="clients__container">

		<?
			$num = 0;
		?>

		<? for ($i=0; $i < 16; $i++) { ?>
		<? $num++; ?>
		<div class="clients__item">
			<img src="./assets/images/clients/clients<? echo $num; ?>.png" alt="1">
		</div>

		<?php } ?>

	</div>
</section>

<section class="section cooperation lazy" data-bg="./assets/images/hero--bg.png">

	<div class="cooperation__img">
		<img src="./assets/images/cooperation__img.png" alt="Катушка ниток с линейкой">
	</div>

	<div class="container-fluid">

		<div class="cooperation__row tabs">

			<div class="cooperation__col">
				<div class="cooperation__title">
					<h2>
						Сотрудничать с нами <span>максимально просто</span>
					</h2>
				</div>

				<ul class="cooperation__nav -mobile">
					<li class="cooperation__nav_item active" data-show="1" data-nav-parent="1"></li>
					<li class="cooperation__nav_item" data-show="2" data-nav-parent="2"></li>
					<li class="cooperation__nav_item" data-show="3" data-nav-parent="3"></li>
					<li class="cooperation__nav_item" data-show="4" data-nav-parent="4"></li>
					<li class="cooperation__nav_item" data-show="5" data-nav-parent="5"></li>
				</ul>

				<div class="cooperation__content">
					<div class="cooperation__numbers_nav">
						<div class="icon-arrow-left arrow-number arrow-number-left js--arrow-left disabled"></div>
						<ul class="cooperation__numbers">
							<li class="cooperation__numbers_item active" data-text="01" data-nav-child="1"><span>1</span></li>
							<li class="cooperation__numbers_item" data-text="02" data-nav-child="2"><span>2</span></li>
							<li class="cooperation__numbers_item" data-text="03" data-nav-child="3"><span>3</span></li>
							<li class="cooperation__numbers_item" data-text="04" data-nav-child="4"><span>4</span></li>
							<li class="cooperation__numbers_item" data-text="05" data-nav-child="5"><span>5</span></li>
						</ul>
						<div class="icon-arrow-right arrow-number arrow-number-right js--arrow-right"></div>
					</div>


					<div class="cooperation__info_container">

						<div class="cooperation__info_item active" data-item="1">
							<p>
								Оставляете заявку на сайте.
								Мы свяжемся с вами в течение рабочего дня и обсудим проект.
							</p>

							<div class="cooperation__info_img">
								<picture>
									<source data-srcset="./assets/images/webp/cooperation_img.webp" type="image/webp">
									<source data-srcset="./assets/images/./assets/images/cooperation/cooperation_img.png" type="image/png">
									<img class="lazy" data-src="./assets/images/./assets/images/cooperation/cooperation_img.png" alt="Ноутбук">
								</picture>
							</div>
						</div>

						<div class="cooperation__info_item" data-item="2">
							<p>
								Согласовываем дизайн, стоимость и сроки. При необходимости заключаем договор
							</p>

							<div class="cooperation__info_img">
								<picture>
									<source data-srcset="./assets/images/webp/cooperation_img2.webp" type="image/webp">
									<source data-srcset="./assets/images/./assets/images/cooperation/cooperation_img2.png" type="image/png">
									<img class="lazy" data-src="./assets/images/./assets/images/cooperation/cooperation_img2.png" alt="Брендбук">
								</picture>
							</div>
						</div>

						<div class="cooperation__info_item" data-item="3">
							<p>
								Вносите предоплату 50% любым удобным способом: наличными, на карту, на расчетный счет
							</p>

							<div class="cooperation__info_img">
								<picture>
									<source data-srcset="./assets/images/webp/cooperation_img3.webp" type="image/webp">
									<source data-srcset="./assets/images/./assets/images/cooperation/cooperation_img3.png" type="image/png">
									<img class="lazy" data-src="./assets/images/./assets/images/cooperation/cooperation_img3.png" alt="Пластиковая карта">
								</picture>
							</div>
						</div>

						<div class="cooperation__info_item" data-item="4">
							<p>
								Отшиваем заказ и отправляем вам после получения остатка суммы. Доставляем с помощью Почты России, CDEK и “Деловых линий”
							</p>

							<div class="cooperation__info_img">
								<picture>
									<source data-srcset="./assets/images/webp/cooperation_img4.webp" type="image/webp">
									<source data-srcset="./assets/images/./assets/images/cooperation/cooperation_img4.png" type="image/png">
									<img class="lazy" data-src="./assets/images/./assets/images/cooperation/cooperation_img4.png" alt="Минивен">
								</picture>
							</div>
						</div>

						<div class="cooperation__info_item" data-item="5">
							<p>
								Вы получаете заказ, и готовы покорять сердца и умы ваших клиентов!
							</p>

							<div class="cooperation__info_img">
								<picture>
									<source data-srcset="./assets/images/webp/cooperation_img5.webp" type="image/webp">
									<source data-srcset="./assets/images/./assets/images/cooperation/cooperation_img5.png" type="image/png">
									<img class="lazy" data-src="./assets/images/./assets/images/cooperation/cooperation_img5.png" alt="Две девушки">
								</picture>
							</div>
						</div>
					</div>


				</div>
			</div>

			<div class="cooperation__col">

				<ul class="cooperation__nav js--not-mobile">
					<li class="cooperation__nav_item active" data-show="1" data-nav-parent="1"></li>
					<li class="cooperation__nav_item" data-show="2" data-nav-parent="2"></li>
					<li class="cooperation__nav_item" data-show="3" data-nav-parent="3"></li>
					<li class="cooperation__nav_item" data-show="4" data-nav-parent="4"></li>
					<li class="cooperation__nav_item" data-show="5" data-nav-parent="5"></li>
				</ul>

			</div>
		</div>

	</div>

</section>

<section class="section footer_form">
	<div class="footer_form__bg"></div>
	<div class="footer_form__img">
		<picture>
			<source data-srcset="./assets/images/webp/footer_girl.webp" type="image/webp">
			<source data-srcset="./assets/images/footer_girl.png" type="image/png">
			<img class="lazy" data-src="./assets/images/footer_girl.png" alt="Девушка">
		</picture>
	</div>

	<div class="footer_form__line">
		<img src="./assets/images/footer_line.png" alt="Линия шва">
	</div>
	<div class="footer_form__line --bottom --opacity">
		<img src="./assets/images/footer_line.png" alt="Линия шва">
	</div>


	<div class="container-fluid">
		<form>
			<div class="footer_form__row">
				<div class="footer_form__col">

					<div class="footer_form__title">
						<h2>
							<span>Оставьте</span> заявку,
						</h2>
						<p>
							ответив на несколько вопросов
						</p>
					</div>

					<h4 class="footer_form__step">
						Шаг 1
					</h4>

					<div class="footer_form__wrap">
						<div class="footer_form__inputs">
							<p>
								Какая продукция Вас заинтересовала
							</p>


							<div class="footer_form__inputs_row">

								<div class="footer_form__inputs_col">

									<div class="checkbox footer_form__checkbox">
										<input type="checkbox" class="input" name="1" id="checkbox-1">
										<label class="checkbox__label" for="checkbox-1">
											<span class="checkbox__box"></span>
											Спортивный костюм
										</label>
									</div>
									<div class="checkbox footer_form__checkbox">
										<input type="checkbox" class="input" name="1" id="checkbox-2">
										<label class="checkbox__label" for="checkbox-2">
											<span class="checkbox__box"></span>
											Толстовка
										</label>
									</div>
									<div class="checkbox footer_form__checkbox">
										<input type="checkbox" class="input" name="1" id="checkbox-3">
										<label class="checkbox__label" for="checkbox-3">
											<span class="checkbox__box"></span>
											Бомбер
										</label>
									</div>
									<div class="checkbox footer_form__checkbox">
										<input type="checkbox" class="input" name="1" id="checkbox-4">
										<label class="checkbox__label" for="checkbox-4">
											<span class="checkbox__box"></span>
											Свитшот
										</label>
									</div>


								</div>

								<div class="footer_form__inputs_col">
									<div class="checkbox footer_form__checkbox">
										<input type="checkbox" class="input" name="1" id="checkbox-5">
										<label class="checkbox__label" for="checkbox-5">
											<span class="checkbox__box"></span>
											Футболка
										</label>
									</div>
									<div class="checkbox footer_form__checkbox">
										<input type="checkbox" class="input" name="1" id="checkbox-6">
										<label class="checkbox__label" for="checkbox-6">
											<span class="checkbox__box"></span>
											Поло
										</label>
									</div>
									<div class="checkbox footer_form__checkbox">
										<input type="checkbox" class="input" name="1" id="checkbox-7">
										<label class="checkbox__label" for="checkbox-7">
											<span class="checkbox__box"></span>
											Другое
										</label>
									</div>
								</div>

							</div>

						</div>

						<div class="footer_form__inputs">
							<p>
								Для кого подбираете:
							</p>

							<div class="footer_form__inputs_row --space-between">
								<div class="checkbox">
									<input type="checkbox" class="input" name="1" id="checkbox-8">
									<label class="checkbox__label" for="checkbox-8">
										<span class="checkbox__box"></span>
										Взрослых
									</label>
								</div>

								<div class="checkbox">
									<input type="checkbox" class="input" name="1" id="checkbox-9">
									<label class="checkbox__label" for="checkbox-9">
										<span class="checkbox__box"></span>
										Детей
									</label>
								</div>

								<div class="checkbox">
									<input type="checkbox" class="input" name="1" id="checkbox-10">
									<label class="checkbox__label" for="checkbox-10">
										<span class="checkbox__box"></span>
										И детей, и взрослых
									</label>
								</div>
							</div>

						</div>

						<div class="footer_form__inputs">
							<p>
								Для кого подбираете:
							</p>


							<div class="footer_form__inputs_row --flex-wrap">
								<div class="checkbox checkbox--m">
									<input type="checkbox" class="input" name="1" id="checkbox-11">
									<label class="checkbox__label" for="checkbox-11">
										<span class="checkbox__box"></span>
										5-20
									</label>
								</div>

								<div class="checkbox checkbox--m">
									<input type="checkbox" class="input" name="1" id="checkbox-12">
									<label class="checkbox__label" for="checkbox-12">
										<span class="checkbox__box"></span>
										21-100
									</label>
								</div>

								<div class="checkbox checkbox--m">
									<input type="checkbox" class="input" name="1" id="checkbox-13">
									<label class="checkbox__label" for="checkbox-13">
										<span class="checkbox__box"></span>
										>100
									</label>
								</div>

								<div class="checkbox checkbox--width100">
									<input type="checkbox" class="input" name="1" id="checkbox-14">
									<label class="checkbox__label" for="checkbox-14">
										<span class="checkbox__box"></span>
										Пробный образец
									</label>
								</div>
							</div>



						</div>

						<div class="footer_form__inputs footer_form__textarea">
							<textarea name="text" class="textarea" placeholder="Ваше сообщение"></textarea>
						</div>
					</div>


				</div>
				<div class="footer_form__col">

					<div class="form_wrap">

						<h4 class="footer_form__step --white">
							Шаг 2
						</h4>

						<p class="form__heading" >
							<span>Оставьте свои контакты</span>, и мы свяжемся с вами в ближайшее время
						</p>

						<div class="input_wrapper">
							<div class="input_container">
								<label class="input_label" for="input-1">
								<div class="icon-user form-icon"></div>
									Ваше имя
								</label>
								<input type="text" id="input-1">
							</div>
							<div class="input_container">
								<label class="input_label" for="input-2">
									<div class="icon-phone form-icon"></div>
									Ваш телефон
								</label>
								<input type="text" id="input-2">
							</div>
							<div class="input_container">
								<label class="input_label" for="input-3">
									<div class="icon-email form-icon form-icon--small_size"></div>
									Ваш e-mail
								</label>
								<input type="text" id="input-3">
							</div>
						</div>

						<div class="agreement-check lt animate-top">
							<input class="agreement" type="checkbox" checked="checked" value="Согласие на обработку данных" name="Agreement">
							<label class="agreement-label">
								<span class="check"></span>Я даю свое согласие на обработку персональных данных и соглашаюсь с <a href="" >политикой конфиденциальности</a>
							</label>
						</div>

						<button class="btn" type="submit">
							<span>Отправить</span>
						</button>

					</div>



				</div>
			</div>
		</form>
	</div>
</section>



<? include $_SERVER['DOCUMENT_ROOT'].'/app/footer.php' ?>
